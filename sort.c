#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <locale.h>

#include "cs402.h"
#include "my402list.h"

typedef struct {
                    char symbol;
                    time_t timestamp;
                    int amount;
                    char description[25];
               } transactions;

/* ----------------------- Functions() ----------------------- */

static
void BubbleForward(My402List *List, My402ListElem **pp_elem1, My402ListElem **pp_elem2) {
    My402ListElem *elem1=(*pp_elem1), *elem2=(*pp_elem2);
    void *obj1=elem1->obj, *obj2=elem2->obj;
    My402ListElem *elem1prev=My402ListPrev(List, elem1);
    My402ListElem *elem2next=My402ListNext(List, elem2);

    My402ListUnlink(List, elem1);
    My402ListUnlink(List, elem2);
    if (elem1prev == NULL) {
        (void)My402ListPrepend(List, obj2);
        *pp_elem1 = My402ListFirst(List);
    } else {
        (void)My402ListInsertAfter(List, obj2, elem1prev);
        *pp_elem1 = My402ListNext(List, elem1prev);
    }
    if (elem2next == NULL) {
        (void)My402ListAppend(List, obj1);
        *pp_elem2 = My402ListLast(List);
    } else {
        (void)My402ListInsertBefore(List, obj1, elem2next);
        *pp_elem2 = My402ListPrev(List, elem2next);
    }
}

static
void BubbleSortForwardList(My402List *List, int num_items) {
    My402ListElem *elem=NULL;
    int i=0;

    for (i=0; i < num_items; i++) {
        int j=0, something_swapped=FALSE;
        My402ListElem *next_elem=NULL;

        for (elem=My402ListFirst(List), j=0; j < num_items-i-1; elem=next_elem, j++) {
            next_elem = My402ListNext(List, elem);
            transactions *curr_transact = elem->obj;
            transactions *next_transact = next_elem->obj;
            time_t cur_val=(int)(curr_transact->timestamp), next_val=(int)(next_transact->timestamp);

            if ( cur_val == next_val ) {
                fprintf(stderr, "Duplicate timestamp in input\n");
                exit(0);
            }

            if (cur_val > next_val) {
                BubbleForward(List, &elem, &next_elem);
                something_swapped = TRUE;
            }
        }
        if (!something_swapped) break;
    }
}

int str2int(char *string, int line_number) {
    int number;
    char *dot_ptr = strchr(string, '.');
    *dot_ptr++ = '\0';
    if ( strlen(dot_ptr) != 2 ) {
        fprintf(stderr, "Malformed transaction time in input line %d\n", line_number);
        exit(0);
    }
    number = ((int) atoi(string)) * 100;
    number += (int) atoi(dot_ptr);
    return(number);
}

/* ----------------------- main() ----------------------- */
int main ( int argc, char **argv) {

    //------ Check for commandline inputs and complain if its Bad.----//
    //------ Make sure file is good and readable. --------------------//
    //------ Complain if directory is given as the input -------------//

    FILE *file_handler;
    if ( argc < 2 ) {
        fprintf(stderr, "Improper usage\n");
        exit(0);
    } else if ( argc < 3 ) {
        if ( strcmp(*(argv+1), "sort") != 0 ) {
            fprintf(stderr, "Malformed command\n");
            exit(0);
        } else {
            file_handler = stdin;
        }
    } else {
        if ( strcmp(*(argv+1), "sort") != 0 ) {
            fprintf(stderr, "Malformed command\n");
            exit(0);
        }

        if ( access(*(argv+2), F_OK) == -1 ) {
            fprintf(stderr, "Input file %s doesn't exist\n", *(argv+2));
            exit(0);
        }

        struct stat file_check;
        stat(*(argv+2), &file_check);
        if ( S_ISDIR(file_check.st_mode) ) {
            fprintf(stderr, "Input file %s is a directory\n", *(argv+2));
            exit(0);
        }

        if ( access(*(argv+2), R_OK) == -1 ) {
            fprintf(stderr, "Cannot read input file %s, permission denied\n", *(argv+2));
            exit(0);
        }

        file_handler = fopen(*(argv+2), "r");
        if ( file_handler == NULL) {
            fprintf(stderr, "Input file %s cannot be open\n", *(argv+2));
            exit(0);
        }
    }

    //---- Read file line by line and complain if it's greater than it suppose to be -----//
    //---- Make sure if input data is in the right format, if not complain and exit ------//
    //---- Append this data to the double linked list to sort. ---------------------------//

    My402List list;
    (void)My402ListInit(&list);
    char read_buffer[1026];
    int line_number = 1;

    while ( fgets(read_buffer, sizeof(read_buffer), file_handler) != NULL ) {
        transactions *transaction_row = malloc(sizeof(transactions));
        char *start_ptr = read_buffer;
        char *tab_ptr = strchr(start_ptr, '\t');
        int elem_num = 0;
        if ( strlen(start_ptr) > 1024 ) {
            fprintf (stderr,"Malformed input at line %d\n", line_number);
            exit(0);
        }
        while ( tab_ptr != NULL ) {
            *tab_ptr++ = '\0';
            if ( elem_num == 0 ) {
                if ( strlen(start_ptr) != 1 || (*start_ptr != '+' && *start_ptr != '-') ) {
                    fprintf(stderr, "Malformed transaction type in input line %d\n", line_number);
                    exit(0);
                }
                transaction_row->symbol = *start_ptr;
            } else if ( elem_num == 1 ) {
                if ( strlen(start_ptr) > 10 ) {
                    fprintf(stderr, "Malformed transaction time in input line %d\n", line_number);
                    exit(0);
                }
                time_t temp = (time_t) atoi(start_ptr);
                if ( temp > time(NULL) ) {
                    fprintf(stderr, "Malformed transaction time in input line %d\n", line_number);
                    exit(0);
                }
                transaction_row->timestamp = (time_t) atoi(start_ptr);
            } else if ( elem_num == 2 ) {
                transaction_row->amount = str2int(start_ptr, line_number);
                if ( transaction_row->amount >= 1000000000 || transaction_row->amount < 0 ) {
                    fprintf(stderr, "Malformed transaction amount in input line %d\n", line_number);
                    exit(0);
                }
            } else {
                fprintf(stderr, "Malformed input at line %d\n", line_number);
                exit(0);
            }
            elem_num++;
            start_ptr = tab_ptr;
            tab_ptr = strchr(start_ptr, '\t');
        }
        if ( elem_num != 3 ) {
            fprintf(stderr, "Malformed input at line %d\n", line_number);
            exit(0);
        }
        tab_ptr = strchr(start_ptr, '\n');
        *tab_ptr-- = '\0';
        if ( strlen(start_ptr) > 0 ) {
            while ( *start_ptr == ' ' ) {
                *start_ptr++ = '\0';
            }
            while ( *tab_ptr == ' ' ) {
                *tab_ptr-- = '\0';
            }
            tab_ptr++;
            if ( strlen(start_ptr) == 0 ) {
                fprintf(stderr, "Malformed transaction description in input line %d\n", line_number);
                exit(0);
            }
        } else {
            fprintf(stderr, "Malformed transaction description in input line %d\n", line_number);
            exit(0);
        }
        while ( strlen(start_ptr) < 24 ) {
            *tab_ptr++ = ' ';
            *tab_ptr = '\0';
        }
        while ( strlen(start_ptr) > 24 ) *tab_ptr-- = '\0';

        strncpy(transaction_row->description, start_ptr, strlen(start_ptr)+1);
        (void)My402ListAppend(&list, transaction_row);
        line_number++;
    }
    fclose(file_handler);

    BubbleSortForwardList(&list, list.num_members);

    //------- Print data in the required format ----------------------------------//
    //------- Using 'en_US' encoding to represent numbers with comma separated ---//

    int i = 0;
    double balance_amount = 0.00;
    My402ListElem *elem = My402ListFirst(&list);

    printf("+-----------------+--------------------------+----------------+----------------+\n");
    printf("|       Date      | Description              |         Amount |        Balance |\n");
    printf("+-----------------+--------------------------+----------------+----------------+\n");
    while ( i < list.num_members ) {
        transactions *transaction_row = elem->obj;
        double amount = (double) transaction_row->amount/100;
        if ( transaction_row->symbol == '+' ) {
            balance_amount += amount;
        } else {
            balance_amount -= amount;
        }

        char *date = ctime(&(transaction_row->timestamp));
        char *fix_ptr = strrchr(date, ' ');
        *fix_ptr++ ='\0';
        char *year = fix_ptr;
        fix_ptr = strchr(year, '\n');
        *fix_ptr = '\0';
        fix_ptr = strrchr(date, ' ');
        *fix_ptr = '\0';
        printf ("| %s %s | %24s | ", date, year, transaction_row->description);
        setlocale(LC_NUMERIC, "en_US");

        if ( transaction_row->symbol == '-' ) {
            if (amount >= 10000000 ) {
                printf ("(\?,\?\?\?,\?\?\?.\?\?) | ");
            } else {
                printf ("(%'12.2lf) | ", amount);
            }
        } else {
            if ( amount >= 10000000 ) {
                printf (" \?,\?\?\?,\?\?\?.\?\?  | ");
            } else {
                printf (" %'12.2lf  | ", amount);
            }
        }
        if ( balance_amount > 0 ) {
            if ( balance_amount >= 10000000 ) {
                printf (" \?,\?\?\?,\?\?\?.\?\?  |");
            } else {
                printf (" %'12.2lf  |", balance_amount);
            }
        } else {
            if ( balance_amount <= -10000000 ) {
                printf ("(\?,\?\?\?,\?\?\?.\?\?) |");
            } else {
                printf ("(%'12.2lf) |", (balance_amount * (-1)));
            }
        }
        printf("\n");
        i++;
        elem = My402ListNext(&list, elem);
    }
    printf("+-----------------+--------------------------+----------------+----------------+\n");
    return(0);
}
